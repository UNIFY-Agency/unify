-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 20-Nov-2020 às 18:05
-- Versão do servidor: 5.7.31
-- versão do PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `una`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `exp`
--

DROP TABLE IF EXISTS `exp`;
CREATE TABLE IF NOT EXISTS `exp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pt` text NOT NULL,
  `en` text NOT NULL,
  `es` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `exp`
--

INSERT INTO `exp` (`id`, `pt`, `en`, `es`) VALUES
(1, 'inicio', 'home', 'começo'),
(2, 'serviços', 'services', 'servicios'),
(3, 'projetos', 'Projects', 'Proyectos'),
(4, 'Clientes', 'Clients', 'Clientas'),
(5, 'Equipe', 'Meet the Team', 'Equipo'),
(6, 'Contacto', 'Contact Us', 'Contacta');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lang`
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `iso` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `principal` tinyint(1) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `lang`
--

INSERT INTO `lang` (`id`, `name`, `iso`, `slug`, `principal`, `ativo`) VALUES
(4, 'Português', 'pt', 'lang-pt', 1, 0),
(5, 'Inglês', 'en', 'lang-en', 0, 0),
(6, 'Espanhol', 'es', 'lang-es', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `marcas`
--

DROP TABLE IF EXISTS `marcas`;
CREATE TABLE IF NOT EXISTS `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `segmento` varchar(255) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `criado` date NOT NULL,
  `atualizado` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `marcas`
--

INSERT INTO `marcas` (`id`, `nome`, `segmento`, `codigo`, `descricao`, `criado`, `atualizado`) VALUES
(1, 'Sylvac', 'Sylvac', 'S', '', '2020-10-20', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `menu`
--

INSERT INTO `menu` (`id`, `name`, `slug`) VALUES
(1, 1, 'home'),
(2, 2, 'services'),
(3, 3, 'projects'),
(4, 4, 'clients'),
(5, 5, 'meet-the-team'),
(6, 6, 'contact-us');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

DROP TABLE IF EXISTS `produtos`;
CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referencia` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `categoria` int(11) NOT NULL,
  `grupo` tinyint(1) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `sku` varchar(10) NOT NULL,
  `preco` decimal(10,2) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `criado` date NOT NULL,
  `atualizado` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `referencia`, `nome`, `categoria`, `grupo`, `descricao`, `sku`, `preco`, `ativo`, `image_url`, `tipo`, `id_marca`, `criado`, `atualizado`) VALUES
(1, 'PS15', 'Faixa de medição 10mm / Horizontal', 1, 0, '• Pequena mesa de bancada horizontal projetada para verificar de forma fácil e rápida peças pequenas de até 10 mm.<br/>\r\n• Fuso de medição guiado por rolamento linear de esferas.<br/>\r\n• O valor medido será exibido por um medidor com mostrador digital Syl', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201509232871cdf81182d6f939b98e2e919811afcab6f022.png', '', 1, '2020-10-20', '2020-10-20'),
(2, 'PS15 BV', 'Faixa de medição 10mm / Vertical', 1, 0, '• Pequena mesa de bancada vertical projetada para verificar facilmente e rapidamente peças pequenas de até 20 mm\r\n• Fuso de medição guiado por rolamento de esferas linear\r\n• O valor medido será exibido por um medidor com mostrador digital Sylvac de 12,5 m', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201439371f804f205f8057afc7fe652c58fac0defd0efeba.png', '', 1, '2020-10-20', '0000-00-00'),
(3, 'PS16 V2', 'Faixa de medição 25 mm & 50 mm / Standard', 1, 0, '• Nova série de bancada PS16 com design moderno e transmissão Bluetooth® integrada\r\n• As mesas de bancada PS16 VS / VP são projetadas para verificar dimensões de até 50 mm (depende do modelo)\r\n• Estas mesas de bancada podem ser equipad                    ', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020152600cbd4740fbd9775a76ba72ff354aecffc8d842b31.png', '', 1, '2020-10-20', '2020-10-20'),
(4, 'PS16 V2 LV', 'Faixa de medição Ø12-62mm, Ø24-70mm, Ø0-50mm / LV', 1, 0, '• As versões LV são projetadas para verificar as dimensões internas de até 70 mm ou as dimensões externas de até 50 mm (depende do modelo)\r\n• Bluetooth® integrado\r\n• Estas mesas de bancada podem ser equipadas com diferentes tipos de mesas para atender às ', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020152659c45c21acc74c222c90afabe3d6da32ab42a0929f.png', '', 1, '2020-10-20', '0000-00-00'),
(5, 'PS16 V2 3-Points', 'Faixa de medição até 12.5mm / 3 Points', 1, 0, '• A versão de 3 pontos é projetada para verificar as dimensões internas de 0,8 mm até 12,5 mm\r\n• Bluetooth® integrado\r\n• Versão personalizada com base nos componentes para medir disponíveis mediante solicitação (fornecer desenho)\r\n• Mesa com ajuste fino n', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020152755faeb0b95185236a54a3ac9145f6ad4229d7aa712.png', '', 1, '2020-10-20', '0000-00-00'),
(6, 'PS16 V2 Goutte', 'Faixa de medição 25 & 50mm / Goutte', 1, 0, '• Versão de goutte projetada para medir pequenos pivôs em placas de relógio (gouttes) ou em peças que requerem uma folga importante para carregar os componentes na mesa\r\n• Bluetooth® integrado\r\n• Ajuste rápido e simples da mesa de liberação e a retirada d', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201528498620ee3dd408319ebff61168939fbec737dd9520.png', '', 1, '2020-10-20', '0000-00-00'),
(7, 'PS17 VS', 'Faixa de medição  0-37mm / VS', 1, 0, '• Bancada de medição com grande flexibilidade de uso\r\n• Bigornas Ø1,5 mm\r\n• Cabeçote ajustável no eixo X (e em Z, depende do modelo)\r\n• Parafuso micrométrico preciso para pré-posicionamento\r\n• Compatibilidade com bigornas Sylvac (Ø1,5mm) e bigornas tipo C', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201529296ca0596fcc1f2e1145ad200c60e189107528ae70.png', '', 1, '2020-10-20', '0000-00-00'),
(8, 'PS17 EP', 'Faixa de medição 0-37mm / Entre-portée', 1, 0, '• Bancada de medição com grande flexibilidade de uso\r\n• Cabeçote ajustável no eixo X e em Z\r\n• Mini indicador para exibir a posição no eixo Z (altura) do contra-ponto\r\n• Entregue com bigornas de metal duro 0,7 mm com 90 ° V para a medida de largura de dis', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020153050ba56814b6d64f652f9da8d53530e3c00b54a76b1.png', '', 1, '2020-10-20', '0000-00-00'),
(9, 'S_Cal EVO Proximity', 'Intervalos de medição: 150 mm / 200 mm / 300 mm', 2, 0, '• Saída de dados de proximidade para USB ou RS232\r\n• Novo display extragrande\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção IP67 de acordo com IEC 60529\r\n• L', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201729222871cdf81182d6f939b98e2e919811afcab6f022.png', '', 1, '2020-10-20', '0000-00-00'),
(10, ' S_Cal EVO Basic', 'Intervalos de medição: 150 mm / 200 mm / 300 mm', 2, 0, '• Paquimetro profissional standard\r\n• Novo display extragrande\r\n• Ligar automático (Sistema SIS)\r\n• Hibernação após 20 min. de nenhum uso. (Sistema SIS)\r\n• Posição memorizada no modo de hibernação (Sistema SIS)\r\n• Velocidade máxima de deslocamento: 2,5 m ', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201730151f804f205f8057afc7fe652c58fac0defd0efeba.png', '', 1, '2020-10-20', '0000-00-00'),
(11, ' S_Cal EVO Smart', 'Intervalos de medição: 150 mm / 200 mm / 300 mm', 2, 0, '• Transmissão de dados BLUETOOTH® Smart integrada\r\n• Novo display extragrande\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção IP67 de acordo com IEC 60529\r\n• L', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020173217cbd4740fbd9775a76ba72ff354aecffc8d842b31.png', '', 1, '2020-10-20', '0000-00-00'),
(12, ' S_Cal EVO Carbide', 'Pinças em metal duro / Alcance 150 mm', 2, 0, '• Transmissão de dados BLUETOOTH® integrada (sem transmissão na opção de haste cilíndrica)\r\n• Novo display extragrande\r\n• Botão programável de acordo com suas necessidades (na versão retangular)\r\n• Protecção para trabalhos pesados com refrigerantes e lubr', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020173314c45c21acc74c222c90afabe3d6da32ab42a0929f.png', '', 1, '2020-10-20', '0000-00-00'),
(13, ' S_Cal EVO Smart Micron', 'Resolução mícron / Alcance 150 mm / Bluetooth®', 2, 0, '• Transmissão de dados  Bluetooth ® integrada\r\n• Novo display extra grande com medição mícron\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção IP67 de acordo co', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020173558faeb0b95185236a54a3ac9145f6ad4229d7aa712.png', '', 1, '2020-10-20', '0000-00-00'),
(14, ' S_Cal EVO Smart EG', 'Medição de Ranhuras externas / Alcance 150mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ® integrada\r\n• Pinças especiais para medir ranhuras e assentamentos externos\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção I', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201737058620ee3dd408319ebff61168939fbec737dd9520.png', '', 1, '2020-10-20', '0000-00-00'),
(15, ' S_Cal EVO Smart IG', 'Medição de Ranhuras internas / Alcance 150mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ® integrada\r\n• Pinças especiais para medir ranhuras e assentamentos internos\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção I', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201738136ca0596fcc1f2e1145ad200c60e189107528ae70.png', '', 1, '2020-10-20', '0000-00-00'),
(16, ' S_Cal EVO Smart DM', 'Medição de profundidades / Alcance 150mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ®  integrada\r\n• Pinças internas longas para medições profundas\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção IP67 de acordo ', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020173922ba56814b6d64f652f9da8d53530e3c00b54a76b1.png', '', 1, '2020-10-20', '0000-00-00'),
(17, ' S_Cal EVO Smart PJ', 'Medição de ranhuras internas / Alcance 150mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ® integrada\r\n• Bigornas polidas redondas intercambiáveis para medir ranhuras internas\r\n• Botão programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificaçã', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201739598063cb0305fc7b811ffc810118b88a504fdca933.png', '', 1, '2020-10-20', '0000-00-00'),
(18, ' S_Cal EVO Form E', 'Pinças de ponta / Alcance 150mm / Bluetooth®', 2, 0, '• Tecnologia Bluetooth® integrada\r\n• Calibre de tamanho médio r5 para medição interna / externa\r\n• Botão programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de proteção IP67 de a', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174035d8f5a10d00f18de17fb33c49780ba25a0dfe6a62.png', '', 1, '2020-10-20', '0000-00-00'),
(19, ' S_Cal EVO Form B', 'Pinça de tamanho médio r5 superior / 300mm / Bluetooth®', 2, 0, '• Tecnologia Bluetooth® integrada\r\n• Calibre de tamanho médio r5 para medição interna / externa com pinças superiores\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação de pr', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174138d49f308c228beed16390649445e798076fcde643.png', '', 1, '2020-10-20', '0000-00-00'),
(20, 'Depth  S_Depth EVO Smart', 'Ponta Ø1,5 mm / Alcance 150, 200 e 300 mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ®integrada\r\n• Medidor de profundidade com ponte 100 mm, ponta Ø1,5 mm e parafuso de fixação\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificaçã', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201742286a6a8dfa120644f0f9907e25de0ad55c2415d2c3.png', '', 1, '2020-10-20', '0000-00-00'),
(21, 'Depth gauge S_Depth EVO Smart TJ', 'Duas pinças de medição fixas / alcance 200, 300 e 500 mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ®  integrada\r\n• Medidor de profundidade especial com ponte de 100 mm e parafuso de fixação\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174309c3d6aef3de0829645de93be7a89902f32fbfb4e6.png', '', 1, '2020-10-20', '0000-00-00'),
(22, 'Depth gauge S_Depth EVO Smart SF', 'Medição à face / alcance 200, 300 e 500 mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ® integrada\r\n• Medidor de profundidade especial com ponte de 100 mm e parafuso de fixação\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classificação ', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/20102017435318221799c23647032d971bc6f01367cc566c88b1.png', '', 1, '2020-10-20', '0000-00-00'),
(23, 'Depth gauge S_Depth EVO Smart RP', 'Pino rotativo / alcance 200, 300 e 500 mm / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth ® integrada\r\n• Medidor de profundidade especial duplo com bigorna ajustável e parafuso de fixação\r\n• Programável de acordo com suas necessidades\r\n• Protecção para trabalhos pesados com refrigerantes e lubrificantes, classi', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174431822f2e9cf49c73b367e2d1c1e5f03d5a380c9adc.png', '', 1, '2020-10-20', '0000-00-00'),
(24, 'Ultra light  UL4', 'Ultra leve', 2, 0, '• Transmissão de dados Bluetooth integrada\r\n• Pinças móveis\r\n• Medição interna e externa, agora com pontas internas superiores\r\n• Este sistema garante uma orientação perfeita e protege a viga contra choques\r\n• Exibe tolerâncias superior e inferior\r\n• Inst', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201747516990207cec2474d4ee0ef550ec6bc62d669c728b.png', '', 1, '2020-10-20', '0000-00-00'),
(25, 'Ultra Light  UL4 special jaws A', 'Ultra leve / Pinças Especiais A', 2, 0, '• Transmissão de dados Bluetooth® integrada\r\n• Pinças especiais A, comprimento a especificar a pedido\r\n• Exibe as tolerâncias superior e inferior\r\n• Instrumento extremamente leve\r\n• Proteção IP67', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174828b2b28ee47b3bfff3c74d26cba75c86fc597c9f2a.png', '', 1, '2020-10-20', '0000-00-00'),
(26, 'Ultra Light  UL4 special jaws C', 'Ultra leve / Pinças Especiais C / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth® integrada\r\n• Pinças especiais C, comprimento a especificar a pedido\r\n• Exibe as tolerâncias superior e inferior\r\n• Instrumento extremamente leve\r\n• Proteção IP67', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020174940225b8c7bf22d32fc79bd1fcb8066fdff9d947736.png', '', 1, '2020-10-20', '0000-00-00'),
(27, 'Ultra Light  ULH4', 'Ultra leve / Universal/ Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth® integrada\r\n• Paquímetro digital universal, até 3000mm\r\n• Exibe as tolerâncias superior e inferior\r\n• Instrumento extremamente leve\r\n• Proteção IP67', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/201020175019ed8d87d53ca12945e610c6b33b8f2a8353379769.png', '', 1, '2020-10-20', '0000-00-00'),
(28, 'Ultra Light Depth Gauge ULV4', 'Ultra leve /Medidor de profundidade / Bluetooth®', 2, 0, '• Transmissão de dados Bluetooth® integrada\r\n• Versão de medidor de profundidade, até 1500 mm\r\n• Exibe as tolerâncias superior e inferior\r\n• Instrumento extremamente leve\r\n• Proteção IP67', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/20102017510700e5eaeead6921322592704678c3839c0f2dff8d.png', '', 1, '2020-10-20', '0000-00-00'),
(29, 'Ultra Light Digital Scale ULD4', 'Balança digital / Bluetooth®', 2, 0, '• Tecnologia Bluetooth® integrada\r\n• Versão da escala digital até 3000mm\r\n• Exibe as tolerâncias superior e inferior\r\n• Instrumento extremamente leve\r\n• Proteção IP67', '', '0.00', 1, 'http://localhost/UNIFY/repositories/eurotecnologia/bo/assets/upload/2010201751543219cf8ca381152cedad65f726f17559d200c1ac.png', '', 1, '2020-10-20', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_cat`
--

DROP TABLE IF EXISTS `produtos_cat`;
CREATE TABLE IF NOT EXISTS `produtos_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `criado` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos_cat`
--

INSERT INTO `produtos_cat` (`id`, `nome`, `slug`, `ativo`, `criado`) VALUES
(1, 'Banco de medição', 'banco_de_medi????o', 1, '0000-00-00'),
(2, 'Paquímetros digitais', 'paqu??metros_digitais', 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` char(255) NOT NULL,
  `aniversario` date NOT NULL,
  `genero` tinyint(1) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `nif` int(9) NOT NULL,
  `tel` varchar(12) DEFAULT NULL,
  `tel2` varchar(12) DEFAULT NULL,
  `usuario` char(250) NOT NULL,
  `password` varchar(255) NOT NULL,
  `grupo` int(11) NOT NULL,
  `tipo` int(1) NOT NULL,
  `email_verificado` tinyint(1) NOT NULL,
  `news` tinyint(1) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `ativo` tinyint(4) NOT NULL,
  `deletado` tinyint(4) NOT NULL,
  `criado` date NOT NULL,
  `atualizado` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `aniversario`, `genero`, `email`, `nif`, `tel`, `tel2`, `usuario`, `password`, `grupo`, `tipo`, `email_verificado`, `news`, `logged`, `ativo`, `deletado`, `criado`, `atualizado`) VALUES
(1, 'Renan Pantoja Vilas', '0000-00-00', NULL, 'renan.pantoja@hotmail.com', 294414690, '0', '0', 'renan', '$2y$12$KZEDOUiIondKRihq1m2uxuqtiPWX/M4pBuAHPg/GvfR7Spzeg7H/a', 0, 1, 1, 0, 1, 1, 0, '0000-00-00', '2020-01-14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
